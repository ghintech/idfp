package com.cds.idfp;

import org.idempiere.webservice.client.base.LoginRequest;
import org.idempiere.webservice.client.base.ParamValues;
import org.idempiere.webservice.client.exceptions.WebServiceException;
import org.idempiere.webservice.client.net.WebServiceConnection;
import org.idempiere.webservice.client.request.QueryDataRequest;
import org.idempiere.webservice.client.request.RunProcessRequest;
import org.idempiere.webservice.client.response.RunProcessResponse;
import org.idempiere.webservice.client.response.WindowTabDataResponse;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.cds.idfp.util.HandlerSpooler2;
import com.cds.idfp.util.Hasar;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.idempiere.webservice.client.base.DataRow;
import org.idempiere.webservice.client.base.Enums;
import org.idempiere.webservice.client.base.Enums.WebServiceResponseStatus;
import org.idempiere.webservice.client.base.Field;

public class idfp {
	private static String Vendor = "thefactory";
	private static String PrinterPort = "COM1";
	private static String wsInvoiceType = "Factura";
	private static String wsUpdateInvoiceType = "FacturaUpdate";
	private static String SpoolerFilePath = "C:/IntTFHKA/";
	private static String SpoolerName = "IntTFHKA";
	private static String UpdateDocumentNo ="N";
	private static String Country ="PA";
	private static String Model ="SRP350";

	private static int AD_Client_ID;
	private static int AD_Org_ID;
	private static int AD_Role_ID;
	private static int M_Warehouse_ID;
	private static String UrlBase = "http://localhost:8080";
	private static String UrlBase2 = "erp.casadelsoftware.com";
	private static String UserName;
	private static String UserPass;
	private static String Language;

	private static QueryDataRequest ws;
	private static RunProcessRequest ws2;
	private static int OffSet = 1;
	private static int Limit = 1000;
	private static boolean isCreditMemo=false;
	private static String configFile="erp.properties";
	private static int zlength;
	private static int znumber;
	private static String serialnumber;
	private static int C_FiscalPrintConf_ID;
	private static String OS = "Linux";


	public static String getSerialnumber() {
		return serialnumber;
	}

	public static void setSerialnumber(String serialnumber) {
		idfp.serialnumber = serialnumber;
	}

	public static void main(String[] arg) throws Exception {

		System.out.println("************************************************************");
		System.out.println("**** MEGA POS : SISTEMA COMPUTACIONAL PARA FACTURACION *****");
		System.out.println("************************************************************");
		if(arg.length>0){
			int count =1;
			for (String s: arg) {

				if(count == 1)
					configFile= s.trim();
				count++;

			}
		}
		System.out.println("Leyendo Archivo de configuracion:");
		System.out.println(configFile);
		try {
			File fXmlFile = new File(configFile);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("entry");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;
					switch (eElement.getAttribute("key")) {
					case "Vendor":
						Vendor = eElement.getTextContent();
						//System.out.println(Vendor);
						break;
					case "SpoolerFilePath":
						SpoolerFilePath = eElement.getTextContent();
						//System.out.println(SpoolerFilePath);
						break;
					case "AD_Client_ID":
						AD_Client_ID = Integer.valueOf(eElement.getTextContent());
						//System.out.println(AD_Client_ID);
						break;
					case "AD_Org_ID":
						AD_Org_ID = Integer.valueOf(eElement.getTextContent());
						//System.out.println(AD_Org_ID);
						break;
					case "PrinterPort":
						PrinterPort = eElement.getTextContent();
						//System.out.println(PrinterPort);
						break;
					case "AD_Role_ID":
						AD_Role_ID = Integer.valueOf(eElement.getTextContent());
						//System.out.println(AD_Role_ID);
						break;
					case "M_Warehouse_ID":
						M_Warehouse_ID = Integer.valueOf(eElement.getTextContent());
						//System.out.println(M_Warehouse_ID);
						break;

					case "UrlBase":
						UrlBase = eElement.getTextContent();
						//System.out.println(UrlBase);
						break;

					case "UrlBase2":
						UrlBase2 = eElement.getTextContent();
						//System.out.println(UrlBase2);
						break;
					case "UserName":
						UserName = eElement.getTextContent();
						//System.out.println(UserName);
						break;
					case "UserPass":
						UserPass = eElement.getTextContent();
						//System.out.println(UserPass);
						break;
					case "Language":
						Language = eElement.getTextContent();
						//System.out.println(Language);
						break;
					case "wsInvoiceType":
						wsInvoiceType = eElement.getTextContent();
						//System.out.println(wsInvoiceType);
						break;
					case "UpdateDocumentNo":
						UpdateDocumentNo = eElement.getTextContent();
						//System.out.println(UpdateDocumentNo);
						break;
					case "Country":
						Country = eElement.getTextContent();
						//System.out.println(Country);
						break;
					case "Model":
						Model = eElement.getTextContent();
						//System.out.println(Model);
						break;
					case "C_FiscalPrintConf_ID":
						C_FiscalPrintConf_ID = Integer.valueOf(eElement.getTextContent());
						//System.out.println(C_FiscalPrintConf_ID);
						break;			

					case "OS":
						OS = eElement.getTextContent();
						//System.out.println(OS);
						break;
					case "SpoolerName":
						SpoolerName = eElement.getTextContent();
						//System.out.println(SpoolerName);
						break;

					default:
						break;

					}


				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		createWS();
		System.out.println("Esperando informacion fiscal...");
		while(true) {

			if(!printInvoice()) {
				try {
					TimeUnit.SECONDS.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private static boolean printInvoice() throws Exception {

		WebServiceConnection client = getClient();
		C_Invoice[] invoice=null;
		try {

			do{

				WindowTabDataResponse response = client.sendRequest(ws);
				if (response.getStatus() == WebServiceResponseStatus.Error) {
					System.out.println(response.getErrorMessage());
					return false;
				} else {


					if(response.getNumRows()>0) {
						System.out.println("Total rows: " + response.getTotalRows());
						System.out.println("Num rows: " + response.getNumRows());
						System.out.println("Start row: " + response.getStartRow());
						OffSet+=Limit;
						if(response.getTotalRows()<Limit) {
							OffSet=0;
						}

					} else {
						OffSet=0;
						return false;
					}
					invoice = new C_Invoice[response.getNumRows()];
					for (int i = 0; i < response.getDataSet().getRowsCount(); i++) {
						//System.out.println("Row: " + (i + 1)+" of "+response.getDataSet().getRowsCount());
						invoice[i] = new C_Invoice();


						for (int j = 0; j < response.getDataSet().getRow(i).getFieldsCount(); j++) {
							Field field = response.getDataSet().getRow(i).getFields().get(j);
							//System.out.print(" " + field.getColumn() + " = " + field.getValue() + "  ");
							String fieldValue = String.valueOf(field.getValue());

							switch (field.getColumn()) {
							case "DocumentNo":
								invoice[i].setDocumentNo(fieldValue);
								break;
							case "C_Invoice_ID":
								invoice[i].setC_Invoice_ID(Integer.valueOf(fieldValue));
								break;
							case "WasPrinted":
								invoice[i].setWasPrinted(fieldValue);
								break;
							case "fiscalNumber":
								invoice[i].setFiscalNumber(fieldValue);
								break;
							case "Script":
								invoice[i].setScript(fieldValue);
								break;
							case "C_Invoice_Fiscal_ID":
								invoice[i].setC_Invoice_Fiscal_ID(Integer.valueOf(fieldValue));
								break;
							case "ProductValue":
								invoice[i].setProductvalue(fieldValue);
								break;
							case "ProductName":
								invoice[i].setProductname(fieldValue);
								break;
							case "QtyEntered":
								invoice[i].setQtyentered(BigDecimal.valueOf(Double.valueOf(fieldValue)));
								break;
							case "PriceEntered":
								invoice[i].setPriceentered(BigDecimal.valueOf(Double.valueOf(fieldValue)));
								break;
							case "QtyInvoiced":
								invoice[i].setQtyinvoiced(BigDecimal.valueOf(Double.valueOf(fieldValue)));
								break;
							case "PriceList":
								invoice[i].setPricelist(BigDecimal.valueOf(Double.valueOf(fieldValue)));
								break;
							case "LineDescription":
								invoice[i].setLinedescription(fieldValue);
								break;
							case "AD_Language":
								invoice[i].setAd_language(fieldValue);
								break;

							case "taxrate":
								invoice[i].setTaxrate(BigDecimal.valueOf(Double.valueOf(fieldValue)));
								break;

							case "BPValue":
								invoice[i].setBpvalue(fieldValue);
								break;
							case "BPName":
								invoice[i].setBpname(fieldValue);
								break;
							case "orderdocumentno":
								invoice[i].setOrderdocumentno(fieldValue);
								break;
							case "orderporeference":
								invoice[i].setOrderporeference(fieldValue);
								break;
							case "bplocationname":
								invoice[i].setBplocationname(fieldValue);
								break;
							case "bpcity":
								invoice[i].setBpcity(fieldValue);
								break;
							case "bpregion":
								invoice[i].setBpregion(fieldValue);
								break;
							case "bpaddress1":
								invoice[i].setBpaddress1(fieldValue);
								break;
							case "bpaddress2":
								invoice[i].setBpaddress2(fieldValue);
								break;
							case "bpaddress3":
								invoice[i].setBpaddress3(fieldValue);
								break;
							case "bpaddress4":
								invoice[i].setBpaddress4(fieldValue);
								break;
							case "DocBaseType":
								invoice[i].setDocBaseType(fieldValue);
								break;
							case "DateInvoiced":
								if(fieldValue!=null && fieldValue!="")
									invoice[i].setDateinvoiced(Timestamp.valueOf(fieldValue));
								break;
							case "Description":
								invoice[i].setDescription(fieldValue);
								break;
							case "UPC":
								if(fieldValue!=null)
									invoice[i].setUPC(fieldValue);
								break;
							case "BPTaxID":
								if(fieldValue!=null)
									invoice[i].setBptaxid(fieldValue);
								break;
							default:
								break;
							}
						}

					}

				}
				ws.setOffset(OffSet);
			}while (OffSet!=0);
			if(Vendor.compareTo("thefactory")==0) 
				Receiver(invoice);

			if(Vendor.compareTo("hasar")==0) {
				PrintDocumentHasar(invoice);
			}
		} catch (WebServiceException | NumberFormatException e) {
			System.out.println("Ha ocurrido un error leyendo valores de la factura");
		}
		return true;
	}
	public static boolean PrintDocumentHasar(C_Invoice[] invoice) throws Exception {

		int oldInvoice=0;
		HandlerSpooler2 hsp = null ;
		int top;
		for(int k = 0; k<invoice.length; k++) {
			
			int newInvoice=invoice[k].getC_Invoice_ID();

			if(oldInvoice!=newInvoice) {
				hsp = new HandlerSpooler2(
						PrinterPort, 
						9600, 
						SpoolerFilePath+SpoolerName, 
						SpoolerFilePath+"glog.txt", 
						SpoolerFilePath, 
						"",
						String.valueOf(C_FiscalPrintConf_ID), 
						28);
				if(oldInvoice!=0 ) {
					String desc = new String();


					//	Print Description
					if(invoice[k-1].getDescription() != null
							&& invoice[k-1].getDescription().length() > 0){
						desc = invoice[k-1].getDescription();
					}

					top = (desc.length() -1 >= 0? desc.length() -1: 0);
					if(top > 45)
						top = 45;
					hsp.printTextTrailer(desc.substring(0, top).trim() + " ");

					hsp.printCommand("E");
					invoice[k-1].setFiscalNumber(ReadStatusHasar(hsp,invoice[k-1].getDocBaseType()));
					SendStatusHasar(invoice[k-1]);
				}
				oldInvoice=newInvoice;
				String FiscalDocBaseType="A";
				if(invoice[k].getDocBaseType().compareTo("ARC")==0)
					FiscalDocBaseType="D";



				String nroVoucher = null;
				String codPrinter = null;
				String dateVoucher = null;
				String timeVoucher = null;

				//if(m_DA_ID != 0){
				//MInvoice m_XX_DocAffected = new MInvoice(getCtx(), m_DA_ID, get_TrxName());
				nroVoucher = invoice[k].getDocumentNo();//m_XX_DocAffected.get_ValueAsString("XX_FiscalNo");
				codPrinter = String.valueOf(getC_FiscalPrintConf_ID());//m_XX_DocAffected.get_ValueAsString("PrinterId");
				SimpleDateFormat sdfDate = new SimpleDateFormat("yyMMdd");
				SimpleDateFormat sdfTime = new SimpleDateFormat("hhmmss");
				Timestamp dV = invoice[k].getDateinvoiced();//m_XX_DocAffected.getDateInvoiced();//_ValueAsString("PrinterId");
				dateVoucher = sdfDate.format(dV);
				timeVoucher = sdfTime.format(dV);
				if(nroVoucher == null || nroVoucher.equals(""))
					throw new Exception("No existe Codigo del Documento Afectado");	//	No existe Codigo del Documento Afectado
				if(codPrinter == null || codPrinter.equals(""))
					throw new Exception("No existe Codigo de Impresora en el Documento Afectado");	//	No existe Codigo de Impresora en el Documento Afectado
				//}
				//	Next Sequence
				//MXXDocFiscalPrinterNo pdn = new MXXDocFiscalPrinterNo(getCtx(), m_XX_DocFiscalPrinterNo_ID, get_TrxName());

				String salesOrder = invoice[k].getOrderdocumentno();
				String poReference = invoice[k].getOrderporeference();
				StringBuffer soRefNo = new StringBuffer();

				if(salesOrder == null)
					salesOrder = " ";

				soRefNo.append("*" + "No. Pedido:" + salesOrder + "*");
				soRefNo.append(" ");

				if(poReference == null)
					poReference = " ";

				soRefNo.append("*" + "Ref. Cliente:" + poReference + "*");	

				top = (soRefNo.toString().length() -1 >= 0? soRefNo.toString().length() -1: 0);

				if(top > 45)
					top = 45;

				
				hsp.printTextHeader(soRefNo.toString().substring(0, top));
				hsp.printTextHeader("*" + "Ref. ERP: **" +  nroVoucher+ "**");
				hsp.printHeader(invoice[k].getBpname(), (invoice[k].getBptaxid()!=null)?invoice[k].getBptaxid():invoice[k].getBpvalue(), nroVoucher, codPrinter, dateVoucher, timeVoucher, FiscalDocBaseType);

				

				//	get Partner Location
				//I_C_BPartner_Location address = m_invoice.getC_BPartner_Location();
				//	get Location
				//I_C_Location loc = address.getC_Location();
				//	Concat Location
				StringBuffer nameLoc = new StringBuffer();
				/*if(invoice[k].getBplocationname() != null){
					nameLoc.append(invoice[k].getBplocationname());
					nameLoc.append(", ");	
				}

				if(invoice[k].getBpregion() != null){
					nameLoc.append(invoice[k].getBpregion());
					nameLoc.append(", ");	
				}*/

				if(invoice[k].getBpcity() != null){
					nameLoc.append(invoice[k].getBpcity());
					nameLoc.append(", ");
				}


				StringBuffer nameLoc1 = new StringBuffer();
				if(invoice[k].getBpaddress1() != null){
					nameLoc1.append(invoice[k].getBpaddress1());
					nameLoc1.append(", ");	
				}

				if(invoice[k].getBpaddress2() != null){
					nameLoc1.append(invoice[k].getBpaddress2());
					nameLoc1.append(", ");	
				}

				StringBuffer nameLoc2 = new StringBuffer();
				if(invoice[k].getBpaddress3() != null){
					nameLoc2.append(invoice[k].getBpaddress3());
					nameLoc2.append(", ");
				}

				if(invoice[k].getBpaddress4() != null){
					nameLoc2.append(invoice[k].getBpaddress4());
				}

				StringBuffer soPtSr = new StringBuffer();
				String paymentTerm = invoice[k].getPaymentterm();
				String salesRep = invoice[k].getSalesrepname();

				if(paymentTerm != null){
					soPtSr.append("*" +  "Termino de pago:" + paymentTerm + "*");
					soPtSr.append(" ");	
				}

				if(salesRep != null){
					soPtSr.append("*" +   "Rep. Comercial:" + salesRep + "*");
				}

				top = (soPtSr.toString().length() -1 >= 0? soPtSr.toString().length() -1: 0);

				if(top > 41)
					top = 41;
				hsp.printMessage(soPtSr.toString().substring(0, top) + " ");

				top = (nameLoc.toString().length() -1 >= 0? nameLoc.toString().length() -1: 0);

				if(top > 41)
					top = 41;
				hsp.printMessage( "Direccion:" + nameLoc.toString().substring(0, top));

				top = (nameLoc1.toString().length() -1 >= 0? nameLoc1.toString().length() -1: 0);

				if(top > 41)
					top = 41;

				hsp.printMessage(nameLoc1.toString().substring(0, top) + " ");
				top = (nameLoc2.toString().length() -1 >= 0? nameLoc2.toString().length() -1: 0);

				if(top > 41)
					top = 41;

				hsp.printMessage(nameLoc2.toString().substring(0, top) + " ");
			}
			if(invoice[k].getC_Invoice_ID()==0) {
				hsp = new HandlerSpooler2(
						PrinterPort, 
						9600, 
						SpoolerFilePath+SpoolerName, 
						SpoolerFilePath+"glog.txt", 
						SpoolerFilePath, 
						"",
						String.valueOf(C_FiscalPrintConf_ID), 
						28);
				String desc = new String();
				Hasar objHasar =new Hasar();
				//if(invoice[k].getScript().startsWith("9")) {
					desc=invoice[k].getScript().replace('|',objHasar.FS );
					hsp.printCommand(desc);
					invoice[k].setFiscalNumber(ReadStatusHasar(hsp,invoice[k].getDocBaseType()));
					SendStatusHasar(invoice[k]);
					return true;
				//}
			}
			String nameItem = null;
			String valueItem = null;
			//if(m_InvoiceLine.getM_Product_ID() != 0){
			//	MProduct product = new MProduct(getCtx(), m_InvoiceLine.getM_Product_ID(), get_TrxName());
			nameItem = invoice[k].getProductname();
			valueItem = invoice[k].getUPC()!=null?invoice[k].getUPC():invoice[k].getProductvalue();
			if(invoice[k].getUPC()!=null)
				nameItem=nameItem+" - "+invoice[k].getProductvalue();
			
			//} else if(m_InvoiceLine.getC_Charge_ID() != 0){
			//	MCharge charge = new MCharge(getCtx(), m_InvoiceLine.getC_Charge_ID(), get_TrxName());
			//	nameItem = charge.getName();
			//	valueItem = charge.getName().substring(0, 3);
			//} else
			//	continue;
			//throw new Exception(Msg.translate(getCtx(), "SGNotItem"));	//	No existe Item

			if(invoice[k].getQtyentered().doubleValue() >= 0
					&& invoice[k].getPriceentered().doubleValue() != 0){

				//MTax tax = new MTax(getCtx(), m_InvoiceLine.getC_Tax_ID(), get_TrxName());
				hsp.printLine(nameItem, 
						invoice[k].getQtyinvoiced().doubleValue(), 
						invoice[k].getPriceentered().doubleValue(), 
						invoice[k].getTaxrate().doubleValue(), 
						null, 
						valueItem);
			}else{
				//MTax tax = new MTax(getCtx(), m_InvoiceLine.getC_Tax_ID(), get_TrxName());
				if(invoice[k].getPriceentered().doubleValue() == 0){


					hsp.printLine(nameItem, 
							invoice[k].getQtyinvoiced().doubleValue(), 
							invoice[k].getPricelist().doubleValue(), 
							invoice[k].getTaxrate().doubleValue(), 
							null, 
							valueItem);

					hsp.printLineDisc("Descuento de linea", 
							invoice[k].getQtyinvoiced().doubleValue(), 
							invoice[k].getPricelist().doubleValue(), 
							invoice[k].getTaxrate().doubleValue(), 
							null, 
							valueItem);
				}else if(invoice[k].getQtyinvoiced().doubleValue() < 0){
					hsp.printLineDisc(nameItem, 
							Math.abs(invoice[k].getQtyinvoiced().doubleValue()), 
							invoice[k].getPricelist().doubleValue(), 
							invoice[k].getTaxrate().doubleValue(), 
							null, 
							valueItem);
				}
			}


		}
		String desc = new String();

		//	Print Description
		if(invoice[invoice.length-1].getDescription() != null
				&& invoice[invoice.length-1].getDescription().length() > 0){
			desc = invoice[invoice.length-1].getDescription();
		}


		top = (desc.length() -1 >= 0? desc.length() -1: 0);
		if(top > 45)
			top = 45;
		hsp.printTextTrailer(desc.substring(0, top).trim() + " ");

		hsp.printCommand("E");
		invoice[invoice.length-1].setFiscalNumber(ReadStatusHasar(hsp,invoice[invoice.length-1].getDocBaseType()));
		SendStatusHasar(invoice[invoice.length-1]);
		return true; 

	}



	private static void createWS() {

		ws = new QueryDataRequest();
		ws.setWebServiceType(wsInvoiceType);
		ws.setLogin(getLogin());
		ws.setLimit(Limit);


		//ws.setFilter("(AD_Org_ID=0 OR AD_Org_ID="+getAD_Org_ID()
		//+") AND WasPrinted='N' AND Script IS NOT NULL"
		//+" AND C_FiscalPrintConf_ID = "+getC_FiscalPrintConf_ID());
		DataRow datainvoice = new DataRow();
		datainvoice.addField("AD_Org_ID",getAD_Org_ID());
		datainvoice.addField("WasPrinted","N");
		datainvoice.addField("C_FiscalPrintConf_ID",getC_FiscalPrintConf_ID());
		datainvoice.addField("AD_Language",getLanguage());
		ws.setDataRow(datainvoice);

	}
	private static void updateWS(int RecordID) {

		ws2 = new RunProcessRequest();
		ws2.setWebServiceType("FacturaUpdate");
		ws2.setLogin(getLogin());

	}
	public static LoginRequest getLogin() {
		LoginRequest login = new LoginRequest();
		login.setUser(getUserName());
		login.setPass(getUserPass());
		login.setLang(Enums.Language.valueOf(getLanguage()));
		login.setClientID(getAD_Client_ID());
		login.setRoleID(getAD_Role_ID());
		login.setOrgID(getAD_Org_ID());
		login.setWarehouseID(getM_Warehouse_ID());
		return login;
	}
	public static WebServiceConnection getClient() {
		WebServiceConnection client = new WebServiceConnection();
		client.setAttempts(3);
		client.setTimeout(200000);
		client.setAttemptsTimeout(200000);
		client.setUrl(getUrlBase());
		client.setAppName("PRIM Fiscal Integration");
		return client;
	}

	public static void Receiver(C_Invoice[] invoice) {
		int oldInvoice=0;


		for(int k = 0; k<invoice.length; k++) {
			int newInvoice=invoice[k].getC_Invoice_ID();

			if(oldInvoice!=newInvoice && newInvoice>0 ) {
				if(oldInvoice!=0) {
					Receiver(invoice[k-1]);
				}
				oldInvoice=newInvoice;
			}
			if(invoice.length==1 || k==invoice.length-1) {
				Receiver(invoice[k]);
			}
		}
	}
	public static void Receiver(C_Invoice invoice) {
		boolean invoiceCopy=false;
		boolean fiscalReport=false;
		isCreditMemo=false;
		try {
			String linefeed ="\r\n";
			if (Vendor.compareTo("thefactory")==0){
				FileWriter fos = new FileWriter("factura.txt");

				System.out.println("Almacenando contenido en factura.txt");
				String[] lines =invoice.getScript().split(Pattern.quote("||"));
				if(lines[0].substring(0, 1).compareTo("R")==0) {
					invoiceCopy=true;
					System.out.println("Imprimiendo copia de documento fiscal");
				}
				if(lines[0].substring(0, 1).compareTo("I")==0) {
					fiscalReport=true;
					System.out.println("Es un reporte");
				}
				for(String line :lines) {
					if(line.substring(0, 1).compareTo("d")==0) {
						isCreditMemo=true;
					}
					fos.write(line+linefeed);
				}

				fos.flush();
				fos.close();
				System.out.println("Creacion de archivo completada.");

				System.out.println("Imprimiendo en maquina fiscal thefactory.");



				if("Linux".compareTo(OS)==0) {
					PrintThefactoryLinux();
				}else {
					PrintThefactory();
				}
				System.out.println("Impresion exitosa");
				//TimeUnit.SECONDS.sleep(15);
				if(!invoiceCopy && !fiscalReport) {
					System.out.println("Leyendo Informacion fiscal");
					if("Linux".compareTo(OS)==0) {
						ReadStatusThefactoryLinux();
					}else {
						ReadStatusThefactory();
					}
					//TimeUnit.SECONDS.sleep(10);
					System.out.println("Lectura exitosa. Enviando informacion fiscal");

					SendStatusThefactory(invoice);
					//TimeUnit.SECONDS.sleep(10);
				}
				else if(fiscalReport) {
					System.out.println("Leyendo Informacion fiscal");
					if("Linux".compareTo(OS)==0) {
						ReadStatusThefactoryLinux();
					}else {
						ReadStatusThefactory();
					}
					//TimeUnit.SECONDS.sleep(10);
					System.out.println("Lectura exitosa. Enviando informacion fiscal");
					SendStatusThefactory(invoice);
					//TimeUnit.SECONDS.sleep(10);
				}				
				else{
					SendStatusCopyThefactory(invoice);
					//TimeUnit.SECONDS.sleep(5);
				}
			}

		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}




	public static boolean SendStatusThefactory(C_Invoice invoice) {
		boolean isReport = false;
		if(invoice.getC_Invoice_Fiscal_ID() > 0)
			isReport=true;

		updateWS(invoice.getC_Invoice_ID());
		try {

			boolean StatusExist=false;
			File fileStatus = null;    
			while(!StatusExist){
				fileStatus = new File("Status.txt");    
				StatusExist=fileStatus.exists();
			}
			try{

				FileReader reader = new FileReader(fileStatus);
				BufferedReader br = new BufferedReader(reader); 
				String status; 
				while((status = br.readLine()) != null) {
					if(Country.compareTo("PA")==0) {
						zlength = 1;
						znumber = 0;
						serialnumber = "";
						invoice.setFiscalNumber(status.substring(21,29));
						if(isCreditMemo) {
							invoice.setFiscalNumber(status.substring(34,42));
						}
					}else {
						if(isReport) {
							zlength = status.substring(47,51).length();
							znumber = Integer.parseInt(status.substring(47,51));
							serialnumber = status.substring(66,76);
							if(Model.compareTo("SRP812")==0) {
								zlength = status.substring(77,81).length();
								znumber = Integer.parseInt(status.substring(77,81));
								serialnumber = status.substring(92,102);
							}
						}else {
							zlength = status.substring(47,51).length();
							znumber = Integer.parseInt(status.substring(47,51))+1;
							serialnumber = status.substring(66,76);
							invoice.setFiscalNumber(status.substring(21,29));
							if(isCreditMemo) {
								invoice.setFiscalNumber(status.substring(88,96));
							}
							if(Model.compareTo("SRP812")==0) {
								zlength = status.substring(77,81).length();
								znumber = Integer.parseInt(status.substring(77,81))+1;
								serialnumber = status.substring(92,102);
								invoice.setFiscalNumber(status.substring(21,29));
								if(isCreditMemo) {
									invoice.setFiscalNumber(status.substring(47,55));
								}
							}
						}
					}
					invoice.setWasPrinted("Y");

				} 
				reader.close();
			}catch(Exception e){
				e.printStackTrace();
			}
			//ParamValues data = new ParamValues();
			ParamValues data =new ParamValues();

			if(!isReport) {
				data.addField("C_Invoice_ID",invoice.getC_Invoice_ID());
				data.addField("fiscalNumber",invoice.getFiscalNumber());
				data.addField("C_Invoice_Fiscal_ID", "-1");
			}				
			else {
				data.addField("C_Invoice_ID","-1");
				data.addField("fiscalNumber","-1");
				data.addField("C_Invoice_Fiscal_ID", invoice.getC_Invoice_Fiscal_ID());
			}

			data.addField("WasPrinted",invoice.getWasPrinted());
			data.addField("znumber",String.format("%0" + String.valueOf(zlength) +"d", znumber));
			data.addField("serialnumber",serialnumber);

			if("Y".compareTo(UpdateDocumentNo)==0)
				data.addField("DocumentNo",invoice.getFiscalNumber());
			else
				data.addField("DocumentNo","-1");

			ws2.setParamValues(data);
			WebServiceConnection client = getClient();

			try {
				RunProcessResponse response;
				response = client.sendRequest(ws2);
				System.out.println(response.getErrorMessage());
				if (response.getStatus() == Enums.WebServiceResponseStatus.Error) {

					return false;
				} else{
					return true;

				}
			} catch (WebServiceException | NumberFormatException e) {
			}

		}
		catch (Exception e) {
			System.out.println("No se pudo enviar el archivo de estatus se intentara con el servidor alternativo");
			e.printStackTrace();
			return false;
		}
		return false;
	}
	public static boolean SendStatusHasar(C_Invoice invoice) {
		boolean isReport = false;
		if(invoice.getC_Invoice_Fiscal_ID() > 0)
			isReport=true;

		updateWS(invoice.getC_Invoice_ID());
		try {
			zlength = 1;
			znumber = 0;
			serialnumber = "";
			invoice.setWasPrinted("Y");
			ParamValues data =new ParamValues();

			if(!isReport) {
				data.addField("C_Invoice_ID",invoice.getC_Invoice_ID());
				data.addField("fiscalNumber",invoice.getFiscalNumber());
				data.addField("C_Invoice_Fiscal_ID", "-1");
			}				
			else {
				data.addField("C_Invoice_ID","-1");
				data.addField("fiscalNumber","-1");
				data.addField("C_Invoice_Fiscal_ID", invoice.getC_Invoice_Fiscal_ID());
			}

			data.addField("WasPrinted",invoice.getWasPrinted());
			data.addField("znumber",String.format("%0" + String.valueOf(zlength) +"d", znumber));
			data.addField("serialnumber",serialnumber);

			if("Y".compareTo(UpdateDocumentNo)==0)
				data.addField("DocumentNo",invoice.getFiscalNumber());
			else
				data.addField("DocumentNo","-1");

			ws2.setParamValues(data);
			WebServiceConnection client = getClient();

			try {
				RunProcessResponse response;
				response = client.sendRequest(ws2);
				System.out.println(response.getErrorMessage());
				if (response.getStatus() == Enums.WebServiceResponseStatus.Error) {

					return false;
				} else{
					return true;

				}
			} catch (WebServiceException | NumberFormatException e) {
			}

		}
		catch (Exception e) {
			System.out.println("No se pudo enviar el archivo de estatus se intentara con el servidor alternativo");
			e.printStackTrace();
			return false;
		}
		return false;
	}
	public static boolean SendStatusCopyThefactory(C_Invoice invoice) {
		System.out.println("Actualizando copia");
		updateWS(invoice.getC_Invoice_ID());
		try {

			invoice.setFiscalNumber("-1");
			invoice.setWasPrinted("Y");
			//ParamValues data = new ParamValues();
			ParamValues data =new ParamValues();
			data.addField("C_Invoice_ID",invoice.getC_Invoice_ID());

			data.addField("fiscalNumber",invoice.getFiscalNumber());
			data.addField("WasPrinted",invoice.getWasPrinted());
			data.addField("DocumentNo",invoice.getFiscalNumber());


			ws2.setParamValues(data);
			WebServiceConnection client = getClient();

			try {
				RunProcessResponse response;
				response = client.sendRequest(ws2);
				System.out.println(response.getErrorMessage());
				if (response.getStatus() == Enums.WebServiceResponseStatus.Error) {

					return false;
				} else{
					return true;

				}
			} catch (WebServiceException | NumberFormatException e) {
			}

		}
		catch (Exception e) {
			System.out.println("No se pudo enviar el archivo de estatus se intentara con el servidor alternativo");
			e.printStackTrace();
			return false;
		}
		return false;
	}
	public static void PrintThefactory() {
		String filePath = "IntTFHKA.exe SendFileCmd("+SpoolerFilePath+"factura.txt)";
		try {
			Process p = Runtime.getRuntime().exec(filePath);
			p.waitFor();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		File fileStatus = new File(SpoolerFilePath + "Status_Error.txt"); 
		boolean checkprinter=false;
		if(fileStatus.exists()){
			fileStatus.delete();
		}

		do{
			try {
				String filePath2 = SpoolerFilePath +"IntTFHKA.exe ReadFpStatus()";
				Process p1 = Runtime.getRuntime().exec(filePath2);
				p1.waitFor();
				//usamos filereader para extraer la informacion e insertarla en el ticket
				fileStatus = new File(SpoolerFilePath + "Status_Error.txt");    

				if(fileStatus.exists()){

					// creates a FileReader Object
					FileReader reader = new FileReader(fileStatus);
					BufferedReader br = new BufferedReader(reader); 
					String status; 
					while((status = br.readLine()) != null) { 
						if(status.substring(5,6).compareTo("4")==0){
							checkprinter=true;
						}

					} 
					reader.close();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}while(!checkprinter);
	}
	public static void PrintThefactoryLinux() {
		System.out.println("Entrando a imprimir en linux");
		String filePath = "./tfinulx SendFileCmd factura.txt";
		try {
			Process p = Runtime.getRuntime().exec(filePath);
			p.waitFor(5,TimeUnit.SECONDS);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		File fileStatus = new File("Status_Error.txt"); 
		boolean checkprinter=false;
		if(fileStatus.exists()){
			fileStatus.delete();
		}
		System.out.println("Leyendo status de impresora fiscal");
		do{
			

			try {
				String filePath2 = "./tfinulx ReadFpStatus Status_Error.txt";
				Process p1 = Runtime.getRuntime().exec(filePath2);
				p1.waitFor();
				//usamos filereader para extraer la informacion e insertarla en el ticket
				fileStatus = new File("Status_Error.txt");    

				if(fileStatus.exists()){

					// creates a FileReader Object
					FileReader reader = new FileReader(fileStatus);
					BufferedReader br = new BufferedReader(reader); 
					String status; 
					while((status = br.readLine()) != null) {
						System.out.println(status.substring(16,17));

						if(status.substring(16,17).compareTo("4")==0){
							checkprinter=true;
						}

					} 
					reader.close();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}while(!checkprinter);
	}
	private static void ReadStatusThefactory() {
		String filePath2 = "IntTFHKA.exe UploadStatusCmd(S1)";
		try {
			Process p1 = Runtime.getRuntime().exec(filePath2);
			p1.waitFor();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	private static void ReadStatusThefactoryLinux() {
		String filePath2 = "./tfinulx UploadStatusCmd S1 Status.txt";
		try {
			Process p1 = Runtime.getRuntime().exec(filePath2);
			p1.waitFor(5,TimeUnit.SECONDS);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	private static String ReadStatusHasar(HandlerSpooler2 hsp,String DocBaseType) throws Exception {
		hsp.get_Status();
		File fileStatus=null;
		boolean StatusErrorExist=false;
		int ans=0;
		while(!StatusErrorExist && ans<60){
			fileStatus = new File(SpoolerFilePath.concat("glog.txt"));
			StatusErrorExist=fileStatus.exists();
			TimeUnit.SECONDS.sleep(1);
			ans++;
		}
		//use filereader to extract fiscal info and insert in ticket
		if(StatusErrorExist){
			System.out.println("Guardando la información fiscal...");
			FileReader reader = new FileReader(fileStatus);
			BufferedReader br = new BufferedReader(reader); 
			String status = "",line; 
			//int type = ticket.getTicketType();
			Hasar objHasar =new Hasar();

			while ((line = br.readLine()) != null) {
				status = line;
			}
			String[] statusArray;            
			String aux = "";
			if(status!=null){
				statusArray = status.split(String.valueOf(objHasar.FS));
				if(statusArray.length>=9){
					if(DocBaseType.compareTo("ARI")==0 )        
						aux = statusArray[7];
					else
						aux = statusArray[9];  
				}else {
					System.out.println("La impresora no imprimio correctamente");
				}
				
			}
			br.close();
			System.out.println(aux);
			return aux;
		}
		return null;
	}


	public static int getAD_Client_ID() {
		return AD_Client_ID;
	}
	public static void setAD_Client_ID(int aD_Client_ID) {
		AD_Client_ID = aD_Client_ID;
	}
	public static int getAD_Org_ID() {
		return AD_Org_ID;
	}
	public static void setAD_Org_ID(int aD_Org_ID) {
		AD_Org_ID = aD_Org_ID;
	}
	public static int getAD_Role_ID() {
		return AD_Role_ID;
	}
	public static void setAD_Role_ID(int aD_Role_ID) {
		AD_Role_ID = aD_Role_ID;
	}
	public static int getM_Warehouse_ID() {
		return M_Warehouse_ID;
	}
	public static void setM_Warehouse_ID(int m_Warehouse_ID) {
		M_Warehouse_ID = m_Warehouse_ID;
	}
	public static String getUserName() {
		return UserName;
	}
	public static void setUserName(String userName) {
		UserName = userName;
	}
	public static String getUserPass() {
		return UserPass;
	}
	public static void setUserPass(String userPass) {
		UserPass = userPass;
	}
	public static String getLanguage() {
		return Language;
	}
	public static void setLanguage(String language) {
		Language = language;
	}
	public static String getUrlBase() {
		return UrlBase;
	}
	public static void setUrlBase(String urlBase) {
		UrlBase = urlBase;
	}
	public static String getUrlBase2() {
		return UrlBase2;
	}
	public static void setUrlBase2(String urlBase2) {
		UrlBase2 = urlBase2;
	}

	public static String getWsInvoiceType() {
		return wsInvoiceType;
	}
	public static void setWsInvoiceType(String wsInvoiceType) {
		idfp.wsInvoiceType = wsInvoiceType;
	}

	public static String getWsUpdateInvoiceType() {
		return wsUpdateInvoiceType;
	}
	public static void setWsUpdateInvoiceType(String wsUpdateInvoiceType) {
		idfp.wsUpdateInvoiceType = wsUpdateInvoiceType;
	}
	public static int getC_FiscalPrintConf_ID() {
		return C_FiscalPrintConf_ID;
	}

}
